from django.db import models


class Stone(models.Model):
    name = models.CharField(max_length=150)
    image = models.ImageField()
    description = models.TextField()

    def __str__(self):
        return f'{self.name}'


class Category(models.Model):
    name = models.CharField(max_length=150)
    order = models.IntegerField()

    def __str__(self):
        return f'Category {self.name}'

    class Meta:
        ordering = ['order']


class Item(models.Model):
    name = models.CharField(max_length=150)
    slug = models.CharField(max_length=150, unique=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    image = models.ImageField()
    gist = models.CharField(max_length=150)
    description = models.TextField()
    customizable_stone = models.BooleanField()
    size = models.CharField(max_length=100)
    available_stone = models.ManyToManyField(Stone)

    def __str__(self):
        return f'{self.name}'
