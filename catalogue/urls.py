from django.urls import path
from django.conf.urls import url

from catalogue import views

app_name = 'catalogue'
urlpatterns = [
    path('', views.catalogue, name='index'),
    url(r'(?P<item_slug>[\w\.%+-]+)/$', views.catalogue_item, name='catalogue_item')
]
