$(function() {
    $(".order-list__order-item").each(function() {
        var $collapse_glyph = $(this).find(".panel-heading .glyphicon");
        var $panel_collapse = $(this).find(".panel-collapse");
        $panel_collapse.on("shown.bs.collapse", function() {
            $collapse_glyph
                .removeClass("glyphicon-collapse-down")
                .addClass("glyphicon-collapse-up");
        });
        $panel_collapse.on("hidden.bs.collapse", function() {
            $collapse_glyph
                .removeClass("glyphicon-collapse-up")
                .addClass("glyphicon-collapse-down");
        });
    });
});