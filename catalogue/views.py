from django.shortcuts import render
from catalogue import models


def catalogue_item(request, item_slug):
    item = models.Item.objects.get(slug=item_slug)
    return render(request, 'catalogue_item.html', context={'item': item})


def catalogue(request):
    items = models.Item.objects.all()
    return render(request, 'catalogue.html', context={'items': items})
