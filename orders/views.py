import uuid
from django.shortcuts import render, redirect
import catalogue.models
from orders.models import Order, ORDER_STATUS_CHOICES


def get_and_map_or(d, k, m, default=None):
    v = d.get('client_uuid')
    if v is None:
        return default
    else:
        return m(v)

def get_and_map_or_lazy(d, k, m, default):
    v = d.get('client_uuid')
    if v is None:
        return default()
    else:
        return m(v)


def order(request):
    client_uuid = get_and_map_or(request.COOKIES, 'client_uuid', uuid.UUID)
    orders = []
    if client_uuid is not None:
        orders = Order.objects.filter(client_uuid=client_uuid).all()
    context={
        'has_orders': len(orders) > 0,
        'orders': orders}
    return render(request, 'orders.html', context=context)


def order_add_item(request):
    response = redirect(request.POST.get('back', '/catalogue'))
    item_id_param = request.POST.get('item_id')
    if item_id_param is None:
        return response
    item_id = int(item_id_param)
    client_uuid = get_and_map_or_lazy(request.COOKIES, 'client_uuid', uuid.UUID, uuid.uuid4)
    response.set_cookie('client_uuid', client_uuid)
    item = catalogue.models.Item.objects.get(id=item_id)
    Order.objects.create_or_add_item(client_uuid, item_id)
    return response
