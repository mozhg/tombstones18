from django.contrib import admin
from catalogue import models

admin.site.register(models.Stone)
admin.site.register(models.Category)
admin.site.register(models.Item)
