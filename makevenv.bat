@echo off

cd %~dp0
virtualenv.exe venv
venv\Scripts\pip3.exe install -r requirements.txt