from django.urls import path
from django.conf.urls import url

from orders import views

app_name = 'orders'
urlpatterns = [
    path('', views.order, name='index'),
    path('order_add_item', views.order_add_item, name='order_add_item')]
