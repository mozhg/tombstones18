venv:
	virtualenv venv
	venv/bin/pip install -r requirements.txt

clean:
	rm -r **/migrations
	rm db.sqlite3

db:
	venv/bin/python manage.py makemigrations catalogue orders services
	venv/bin/python manage.py migrate
