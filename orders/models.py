import uuid
import django.contrib.auth.models
import services.models
import catalogue.models
from django.db import models
from functools import reduce


ORDER_STATUS_NOT_COMMITED  = 0
ORDER_STATUS_COMMITED      = 1
ORDER_STATUS_CONFIRMED     = 2
ORDER_STATUS_DELIVERED     = 3


ORDER_STATUS_CHOICES = (
    (ORDER_STATUS_NOT_COMMITED,    'not commited'),
    (ORDER_STATUS_COMMITED,        'commited'),
    (ORDER_STATUS_CONFIRMED,       'confirmed'),
    (ORDER_STATUS_DELIVERED,       'delivered'))


def order_status_str(order_status):
    for i, s in ORDER_STATUS_CHOICES:
        if i == order_status:
            return s
    raise LookupError(f'undefined status state: {order_status}')


class OrderItem(models.Model):
    order = models.ForeignKey('Order', on_delete=models.CASCADE)
    item = models.ForeignKey(catalogue.models.Item, on_delete=models.SET_NULL, null=True)
    quantity = models.IntegerField()


class OrderManager(models.Manager):
    def create_or_add_item(self, client_uuid, item_id):
        item = catalogue.models.Item.objects.get(id=item_id)
        order = None
        try:
            order = self.get(client_uuid=client_uuid)
        except Order.DoesNotExist:
            order = self.create(client_uuid=client_uuid, state=ORDER_STATUS_NOT_COMMITED)
        
        try:
            order_item = OrderItem.objects.get(order=order, item=item)
        except OrderItem.DoesNotExist:
            order_item = OrderItem.objects.create(order=order, item=item, quantity=0)
        order_item.quantity += 1
        order_item.save()


class Order(models.Model):
    client_uuid = models.UUIDField()
    state = models.IntegerField(choices=ORDER_STATUS_CHOICES, default=0)
    services = models.ManyToManyField(services.models.Service)

    objects = OrderManager()

    @property
    def items(self):
        return OrderItem.objects.filter(order=self)

    @property
    def price(self):
        def add(*args):
            return sum(args)
        return reduce(add, [i.catalogue_item.price for i in OrderItem.objects.get(order=self)])

    def __str__(self):
        return f'Order ({order_status_str(self.state)})'
